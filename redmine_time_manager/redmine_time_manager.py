# -*- coding: utf-8 -*-
from pprint import pprint
import json
import pickle
import collections

import questionary
from redminelib import Redmine
from prompt_toolkit.styles import Style
from questionary import Choice, prompt


class Conf:
    def __init__(self, data_file):
        self.data = {}
        self.data_file = data_file

    def serialyze(self):
        with open(self.data_file, "wb") as f:
            pickle.dump(self.data, f)

    def deserialyze(self):
        with open(self.data_file, "rb") as f:
            return pickle.load(f)

    def __enter__(self):
        try:
            self.data = self.deserialyze()
        except json.decoder.JSONDecodeError as e:
            print(e)
        finally:
            return self.data

    def __exit__(self, type, value, traceback):
        self.serialyze()

    def get(self, key):
        self.data.get(key, None)


custom_style_genius = Style(
    [
        ("qmark", "fg:#E91E63 bold"),
        ("question", ""),
        ("selected", "fg:#673AB7 bold"),
        ("answer", "fg:#2196f3 bold"),
    ]
)


class RedmineInterface:
    def __init__(self, server_url, auth={}):
        self.server_url = server_url
        self.redmine = Redmine(self.server_url, **auth)
        self.current_project = None
        self.current_version = None
        self.current_issue = None

    def issue_status(self, **kwargs):
        statuses = redmine.issue_status.all()
        return [(i.name, i.id) for i in statuses]

    @property
    def user_id(self):
        user_id = self.redmine.auth().id
        return user_id

    def project(self):
        import pdb

        pdb.set_trace()
        projects = [(p.__str__(), p) for p in self.redmine.project.all()]
        return collections.OrderedDict(projects)

    def version(self, **kwargs):
        versions = [
            (v.due_date.strftime("%Y-%m-%d-"), v.id)
            for v in self.redmine.version.filter(status="open", **kwargs)
        ]
        versions = sorted(versions, key=lambda x: x[0], reverse=True)
        return collections.OrderedDict(versions)

    def issue(self, **kwargs):
        """ fixed_version_id version_id"""
        issues = [
            (f"#{v.id} {v.subject} {v.status_id} |{v.priority_id}", v.id)
            for v in self.redmine.issue.filter(assigned_to_id=self.user_id, **kwargs)
        ]
        issues = sorted(issues, key=lambda x: x[0].split("|")[1], reverse=True)
        return collections.OrderedDict(issues)


class QuestionManager:
    def __init__(self, server_url, auth={}):
        self.redmine_interface = RedmineInterface(server_url, auth=auth)
        self.default = {}

    def choice(self, object_name, default=None, **kwargs):
        choice_list = getattr(self.redmine_interface, object_name)(**kwargs)
        print(choice_list)
        if default != None:
            choice_list.move_to_end(default, last=False)
        question = questionary.select(
            "Select %s:" % object_name,
            qmark="🍸",
            choices=choice_list.keys(),
            default=default,
            style=custom_style_genius,
        )
        return question.ask(), choice_list

    @classmethod
    def ask_auth_data(*arg, **kwargs):
        site = prompt(
            [
                {
                    "type": "text",
                    "message": "Enter your Redmine instance URL:",
                    "name": "server_url",
                }
            ],
            style=custom_style_genius,
            **kwargs,
        )

        choices_dict = {
            "Login/Password combo": [
                {
                    "type": "text",
                    "message": "Enter your Redmine username:",
                    "name": "login",
                },
                {
                    "type": "password",
                    "message": "Enter your Redmine password:",
                    "name": "password",
                },
            ],
            "API Key": [
                {"type": "text", "message": "Enter your Redmine key:", "name": "key"}
            ],
        }
        question = questionary.select(
            "What you will use for connect Redmine?",
            choices=list(choices_dict.keys()),
            style=custom_style_genius,
            **kwargs,
        )
        return (
            site["server_url"],
            prompt(choices_dict[question.ask()], style=custom_style_genius, **kwargs),
        )

    def ask_key(self, **kwargs):
        questions = ""
        return prompt(questions, style=custom_style_genius, **kwargs)

    def ask_login(self, **kwargs):
        questions = ""
        return prompt(questions, style=custom_style_genius, **kwargs)


def ask_with_default_config(
    question_manager, object_name, configuration_instance, default=None, **kwargs
):
    with configuration_instance as c:
        if not default:
            default = c.get(object_name)
            print(object_name, default)
        response, object_dict = question_manager.choice(
            object_name, default=default, **kwargs
        )
        c[object_name] = response
    return response, object_dict


if __name__ == "__main__":
    project_conf = Conf(".data")
    auth_conf = Conf(".auth")
    while True:
        with auth_conf as c:
            print(c)
            try:
                g = QuestionManager(c.get("server_url"), c.get("auth"))
                user_id = g.redmine_interface.user_id
                break
            except TypeError:
                c["server_url"], c["auth"] = QuestionManager.ask_auth_data()
            except Exception as e:
                print(e)
                c["server_url"], c["auth"] = QuestionManager.ask_auth_data()

    project_response, project_object_dict = ask_with_default_config(
        g, "project", project_conf
    )
    version_response, version_id_list = ask_with_default_config(
        g, "version", project_conf, project_id=project_object_dict[project_response].id
    )
    i = ask_with_default_config(
        g,
        "issue",
        project_conf,
        project_id=project_object_dict[project_response].id,
        fixed_version_id=version_id_list.pop(),
    )

